﻿using System;
using System.Threading.Tasks;
using Autofac;

namespace Barigui.Services.GitlabScrapper
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var build = DependencyInjection.Configure();

            using (var scope = build.BeginLifetimeScope())
            {
                var app = scope.Resolve<Application>();
                await app.Run();
            }
        }
    }
}
