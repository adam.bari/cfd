using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Barigui.Services.GitlabScrapper
{
    public class AppSettings : IAppSettings
    {
        public JsonElement AWS { get; set; }
        public JsonElement Gitlab { get; set; }
        public JsonElement GCloud { get; set; }

        public AppSettings()
        {
            string text = System.IO.File.ReadAllText(@"./appsettings.json");
            var _appSettings = JsonSerializer.Deserialize<IDictionary<string, object>>(text);
            AWS = (JsonElement)_appSettings["AWS"];
            Gitlab = (JsonElement)_appSettings["Gitlab"];
            GCloud = (JsonElement)_appSettings["GCloud"];
        }
    }
}