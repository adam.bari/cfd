
using System.Collections.Generic;
using System.Text.Json;

namespace Barigui.Services.GitlabScrapper
{
    public interface IAppSettings
    {
        public JsonElement AWS { get; set; }
        public JsonElement Gitlab { get; set; }
        public JsonElement GCloud { get; set; }
        // void Load();
    }
}