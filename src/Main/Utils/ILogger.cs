namespace Barigui.Services.GitlabScrapper
{
    public interface ILogger {
        void LogDebug(string text);
    }
}