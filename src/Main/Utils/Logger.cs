using System;

namespace Barigui.Services.GitlabScrapper
{
    public class Logger : ILogger
    {
        public void LogDebug(string text)
        {
            Console.WriteLine(text);
        }
    }
}