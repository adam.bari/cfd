﻿using Autofac;

namespace Barigui.Services.GitlabScrapper
{
    public static class DependencyInjection
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>().AsSelf();
            builder.RegisterType<Logger>().AsSelf();
            builder.RegisterType<S3>().AsSelf();
            builder.RegisterType<Athena>().AsSelf();
            builder.RegisterType<AppSettings>().AsSelf().SingleInstance();
            builder.RegisterType<GitlabOptions>().As<GitlabOptions>().SingleInstance();
            builder.RegisterType<GitlabRestApi>().AsSelf();
            return builder.Build();
        }
    }

}
