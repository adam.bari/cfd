using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text.Json;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;

namespace Barigui.Services.GitlabScrapper
{
    public static class Utils
    {
        public static string GetTextFromJson(JsonElement element, string property)
        {
            return element.GetProperty(property).GetRawText()
                .Substring(1, element.GetProperty(property).GetRawText().Length - 2);
        }

        public static async Task<List<dynamic>> ItarateFetchOnPages(
            Url url,
            Logger logger = null,
            string logText = ""
        )
        {
            var nextPage = "1";
            var items = new List<dynamic>();
            while (!String.IsNullOrEmpty(nextPage))
            {
                try
                {
                    logger.LogDebug($"{logText}getting page {nextPage}");
                    var itemsRequest = await url
                        .SetQueryParams(new { page = nextPage })
                        .GetAsync();
                    nextPage = itemsRequest.Headers["X-Next-Page"];
                    var itemList = await itemsRequest.GetJsonListAsync();
                    items.AddRange(itemList);
                }
                catch (FlurlHttpException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return items;
        }

        public static ExpandoObject AddProperty(ExpandoObject token, string prop, string value)
        {
            token.TryAdd(prop, value);
            return token;
        }
    }
}