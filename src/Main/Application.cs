using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json.Linq;
using System.Dynamic;

namespace Barigui.Services.GitlabScrapper
{
    public class Application : IApplication
    {
        IGitlabApi _gitlabApi;
        S3 _s3;
        private readonly Logger _logger;
        public Application(
            AppSettings appSettings,
            GitlabOptions options,
            GitlabRestApi gitlabApi,
            Logger logger,
            S3 s3
        )
        {
            _logger = logger;
            _s3 = s3;
            _gitlabApi = gitlabApi;
            options.GroupId = Utils.GetTextFromJson(appSettings.Gitlab, "GroupId");
            options.PrivateToken = Utils.GetTextFromJson(appSettings.Gitlab, "PrivateToken");
        }

        public async Task Run()
        {
            _logger.LogDebug($"Starting GitlabScrapper at {DateTime.Now.ToString("o")}");
            // Loading projects list
            List<dynamic> projects = (await _s3.FetchProjects())
                .Select(d => (dynamic)d).ToList();
            if (projects.Count() == 0)
            {
                _logger.LogDebug("Projects list is outdated.");
                projects = await _gitlabApi.FetchProjects();
                await _s3.PutProjects(projects);
            }
            var projectIds = new List<dynamic>();
            foreach (var project in projects)
            {
                projectIds.Add(project.id.ToString());
            }
            // Commits
            var commits = new CommitsYesterdayToday();
            foreach (var projectId in projectIds.Distinct())
            {
                var projectCommits = await _gitlabApi.FetchCommits(projectId);
                commits.Today.AddRange(projectCommits.Today);
                commits.Yesterday.AddRange(projectCommits.Yesterday);
            }
            await _s3.PutCommits(commits);
            // Issues
            var issues = await _gitlabApi.FetchIssues();
            var issuesContent = String.Join(
                Environment.NewLine,
                issues.Select(t => Newtonsoft.Json.JsonConvert.SerializeObject(t))
            );
            await _s3.PutCurrentSnapshot(issuesContent);
            await _s3.PutHistoricalData(issuesContent);
            // Milestones
            var taskList = new List<Task<List<dynamic>>>();
            foreach (var projectId in projectIds)
            {
                taskList.Add(_gitlabApi.FetchProjectMilestones((string)projectId));
            }
            var milestones = await Task.WhenAll(taskList);
            var milestonesContent = String.Join(
                Environment.NewLine,
                milestones.Select(t => Newtonsoft.Json.JsonConvert.SerializeObject(t))
            );
            await _s3.PutMilestones(milestonesContent);
            // Merge Requests
            var mergeRequests = await _gitlabApi.FetchMergeRequests();
            await _s3.PutMergeRequests(mergeRequests);
            // Deployments
            var deployments = new List<dynamic>();
            foreach (var projectId in projectIds)
            {
                var deploymentsOfProject = await _gitlabApi.FetchDeployments(projectId);
                var ds = new List<dynamic>();
                foreach (ExpandoObject d in deploymentsOfProject)
                {
                    ds.Add(Utils.AddProperty(d, "project_id", projectId));
                }
                deployments.AddRange(ds);
            }
            await _s3.PutDeployments(deployments);
            _logger.LogDebug($"Finishing GitlabScrapper at {DateTime.Now.ToString("o")}");
        }
    }
}