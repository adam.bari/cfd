using System.Collections.Generic;

namespace Barigui.Services.GitlabScrapper
{
    public class CommitsYesterdayToday
    {
        public CommitsYesterdayToday()
        {
            Today = new List<dynamic>();
            Yesterday = new List<dynamic>();
        }
        public List<dynamic> Today { get; set; }
        public List<dynamic> Yesterday { get; set; }
    }
}