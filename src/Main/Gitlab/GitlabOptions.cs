﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Barigui.Services.GitlabScrapper
{
    public class GitlabOptions
    {
        public GitlabOptions() {  }

        public string PrivateToken { get; set; }

        public string GroupId { get; set; }
    }
}
