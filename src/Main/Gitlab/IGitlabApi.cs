﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Barigui.Services.GitlabScrapper
{
    public interface IGitlabApi
    {
        Task<List<dynamic>> FetchProjectMilestones(IEnumerable<string> projectIdList);

        Task<List<dynamic>> FetchProjectMilestones(string projectId);

        Task<List<dynamic>> FetchIssues();

        Task<List<dynamic>> FetchProjects();

        Task<List<dynamic>> FetchMergeRequests();

        Task<List<dynamic>> FetchDeployments(string projectId);

        Task<CommitsYesterdayToday> FetchCommits(String projectId);
    }
}
