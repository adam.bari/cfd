using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;


namespace Barigui.Services.GitlabScrapper
{
    public class GitlabRestApi : IGitlabApi
    {
        private string _api = "https://gitlab.com/api/v4/";
        private readonly Logger _logger;
        private readonly GitlabOptions _options;
        private List<dynamic> _projects;

        public GitlabRestApi(Logger logger, GitlabOptions options)
        {
            _logger = logger;
            _options = options;
            _projects = new List<dynamic>();
        }

        public async Task<List<dynamic>> FetchProjectMilestones(IEnumerable<string> projectIdList)
        {
            var projectMilestones = new List<dynamic>();
            await projectIdList.ToObservable()
                .Select(projectId => Observable.FromAsync(() => FetchProjectMilestones(projectId)))
                .Merge(5)
                .Do(t => projectMilestones.AddRange(t));
            return projectMilestones;
        }

        public async Task<List<dynamic>> FetchProjectMilestones(string projectId)
        {
            var url = $"{_api}projects/{projectId}/milestones/?per_page=100&private_token={_options.PrivateToken}";
            var nextPage = "1";
            var milestones = new List<dynamic>();
            while (!String.IsNullOrEmpty(nextPage))
            {
                _logger.LogDebug($"getting milestones for project {projectId} - page {nextPage}");
                var milestoneRequest = await url
                    .SetQueryParams(new { page = nextPage })
                    .GetAsync();
                nextPage = milestoneRequest.Headers["X-Next-Page"];
                var milestoneList = await milestoneRequest.GetJsonListAsync();
                milestones.AddRange(milestoneList);
            }
            return milestones;
        }

        public async Task<List<dynamic>> FetchProjects()
        {
            _logger.LogDebug("Fetching project list");
            _projects.Clear();
            await IterateSubProjects(_options.GroupId);
            return _projects;
        }

        private async Task IterateSubProjects(string group)
        {
            var url = $"{_api}groups/{group}/projects/?private_token={_options.PrivateToken}";
            try
            {
                _logger.LogDebug($"fetching all projects and subgroups in group {group}");
                var result = await url.GetJsonListAsync();
                _projects.AddRange(result);
                url = $"{_api}groups/{group}/subgroups/"
                    .SetQueryParams(new
                    {
                        per_page = 100,
                        private_token = _options.PrivateToken
                    });
                result = await url.GetJsonListAsync();
                foreach (var subgroup in result)
                {
                    var subgroupId = (Int64)((IDictionary<string, Object>)subgroup)["id"];
                    await IterateSubProjects(subgroupId.ToString());
                }
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task<CommitsYesterdayToday> FetchCommits(String projectId)
        {
            var dateToday = DateTime.Today.ToString("o").Substring(0, 10);
            var dateYesterday = DateTime.Today.AddDays(-1).ToString("o").Substring(0, 10);
            var url = $"{_api}/projects/{projectId}/repository/commits"
                .SetQueryParams(new
                {
                    since = DateTime.Today.AddDays(-1).ToString("o"),
                    per_page = 100,
                    private_token = _options.PrivateToken
                });
            var commits = new CommitsYesterdayToday();
            var today = new List<dynamic>();
            var yesterday = new List<dynamic>();
            try
            {
                _logger.LogDebug($"fetching all commits on the last day for the project {projectId}");
                var result = await url.GetJsonListAsync();
                foreach (var commit in result)
                {
                    var commitDate = (DateTime)((IDictionary<string, Object>)commit)["committed_date"];
                    if (commitDate.ToString("o").Substring(0, 10) == dateYesterday)
                        commits.Yesterday.Add(commit);
                    else
                        commits.Today.Add(commit);
                }
            }
            catch (FlurlHttpException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return commits;
        }

        public async Task<List<dynamic>> FetchMergeRequests(){
            var url = $"{_api}merge_requests"
                .SetQueryParams(new {
                    state = "all",
                    updated_after = DateTime.Today.ToString("o"),
                    per_page = 100,
                    private_token = _options.PrivateToken
                });
            return await Utils.ItarateFetchOnPages(url, _logger, "Merge Requests: ");
        }

        public async Task<List<dynamic>> FetchDeployments(string projectId)
        {
            var url = $"{_api}projects/{projectId}/deployments"
                .SetQueryParams(new
                {
                    per_page = 100,
                    updated_after = DateTime.Today.ToString("o"),
                    private_token = _options.PrivateToken
                });
            return await Utils.ItarateFetchOnPages(url, _logger, $"Deployments on proj {projectId}: ");
        }

        public async Task<List<dynamic>> FetchIssues()
        {
            var url = $"{_api}groups/{_options.GroupId}/issues/"
                .SetQueryParams(new
                {
                    state = "all",
                    per_page = 100,
                    private_token = _options.PrivateToken
                });
            return await Utils.ItarateFetchOnPages(url, _logger, "Issues: ");
        }
    }
}
