using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json.Linq;

namespace Barigui.Services.GitlabScrapper
{
    public class S3
    {
        private AppSettings _settings;
        private readonly Logger _logger;
        private static IAmazonS3 _s3Client;
        private Athena _athena;

        public S3(Logger logger, AppSettings settings, Athena athena){
            _settings = settings;
            _logger = logger;
            _athena = athena;
            Console.WriteLine(Utils.GetTextFromJson(_settings.AWS, "Profile"));
            var credentials = new StoredProfileAWSCredentials(Utils.GetTextFromJson(_settings.AWS, "Profile"));
            _s3Client = new AmazonS3Client(credentials);
        }

        public async Task PutMergeRequests(List<dynamic> mergeRequests){
            _logger.LogDebug($"Putting merge requests");
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"merge-requests/merge-requests-{DateTime.Today.ToString("o").Substring(0, 10)}.json",
                ContentBody = String.Join(
                    Environment.NewLine,
                    mergeRequests.Select(p => Newtonsoft.Json.JsonConvert.SerializeObject(p))
                )
            };
            await _s3Client.PutObjectAsync(putRequest);
        }

        public async Task PutDeployments(List<dynamic> deployments){
            _logger.LogDebug($"Putting deployments");
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"deployments/deployments-{DateTime.Today.ToString("o").Substring(0, 10)}.json",
                ContentBody = String.Join(
                    Environment.NewLine,
                    deployments.Select(p => Newtonsoft.Json.JsonConvert.SerializeObject(p))
                )
            };
            await _s3Client.PutObjectAsync(putRequest);
        }

        public async Task PutProjects(List<dynamic> projects)
        {
            _logger.LogDebug($"Putting projects in bucket {Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData")}");
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"projects/projects-{DateTime.Today.ToString("o").Substring(0, 10)}.json",
                ContentBody = String.Join(
                    Environment.NewLine,
                    projects.Select(p => Newtonsoft.Json.JsonConvert.SerializeObject(p))
                )
            };
            await _s3Client.PutObjectAsync(putRequest);
        }

        public async Task<List<JObject>> FetchProjects(){
            _logger.LogDebug("Fetching projects list from cache");
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                    Key = $"projects/projects-{DateTime.Today.ToString("o").Substring(0, 10)}.json"
                };
                using (GetObjectResponse response = await _s3Client.GetObjectAsync(request))
                using (Stream responseStream = response.ResponseStream)
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    string title = response.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
                    string contentType = response.Headers["Content-Type"];
                    string responseBody = reader.ReadToEnd(); // Now you process the response body.
                    var responseList = (List<string>)(responseBody.Split(Environment.NewLine).ToList());
                    var body = responseList.Select(b => Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(b)) ;
                    return body.ToList();
                }
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered. Message:'{0}' when reading object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when reading object", e.Message);
            }
            return new List<JObject>();
        }

        public async Task PutCommits(CommitsYesterdayToday commits)
        {
            _logger.LogDebug("Putting commits in bucket");
            var commitsTodayContent = String.Join(
                Environment.NewLine,
                commits.Today.Select(t => Newtonsoft.Json.JsonConvert.SerializeObject(t))
            );
            var commitsYesterdayContent = String.Join(
                Environment.NewLine,
                commits.Yesterday.Select(t => Newtonsoft.Json.JsonConvert.SerializeObject(t))
            );
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"commits/commits-{DateTime.Today.ToString("o").Substring(0, 10)}.json",
                ContentBody = commitsTodayContent
            };
            await _s3Client.PutObjectAsync(putRequest);
            putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"commits/commits-{DateTime.Today.AddDays(-1).ToString("o").Substring(0, 10)}.json",
                ContentBody = commitsYesterdayContent
            };
            await _s3Client.PutObjectAsync(putRequest);
            _logger.LogDebug("Finished putting current milestones snapshot to bucket");
        }

        public async Task PutMilestones(string content)
        {
            _logger.LogDebug("Emptying milestones bucket");
            var objectList = await _s3Client.ListObjectsV2Async(new ListObjectsV2Request
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Prefix = "milestones/"
            });
            if (objectList.S3Objects.Any())
            {
                var deleteObjectsRequest = new DeleteObjectsRequest
                {
                    BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                    Objects = objectList.S3Objects.Where(t => t.Key != "milestones/")
                        .Select(t => new KeyVersion { Key = t.Key }).ToList()
                };
                await _s3Client.DeleteObjectsAsync(deleteObjectsRequest);
            }
            _logger.LogDebug("Putting milestones json file to bucket");
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"milestones/milestones{DateTime.UtcNow.Ticks}.json",
                ContentBody = content
            };
            await _s3Client.PutObjectAsync(putRequest);
            _logger.LogDebug("Finished putting current milestones snapshot to bucket");
        }

        public async Task PutCurrentSnapshot(string content)
        {
            _logger.LogDebug("Emptying bucket");
            var objectList = await _s3Client.ListObjectsV2Async(new ListObjectsV2Request
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Prefix = "issues/"
            });
            if (objectList.S3Objects.Any())
            {
                var deleteObjectsRequest = new DeleteObjectsRequest
                {
                    BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                    Objects = objectList.S3Objects.Where(t => t.Key != "issues/")
                        .Select(t => new KeyVersion { Key = t.Key }).ToList()
                };
                await _s3Client.DeleteObjectsAsync(deleteObjectsRequest);
            }
            _logger.LogDebug("Putting json file to bucket");
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"issues/issues{DateTime.UtcNow.Ticks}.json",
                ContentBody = content
            };
            await _s3Client.PutObjectAsync(putRequest);
            _logger.LogDebug("Finished putting current snapshot to bucket");
        }

        public async Task PutHistoricalData(string content)
        {
            var snapshot_date = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var objectList = await _s3Client.ListObjectsV2Async(new ListObjectsV2Request
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Prefix = $"issues_historical/snapshot_date={snapshot_date}"
            });
            if (!objectList.S3Objects.Any())
            {
                await _athena.CreateAthenaTablePartition(
                    Utils.GetTextFromJson(_settings.AWS, "AthenaDatabase"),
                    "issues_historical_raw",
                    snapshot_date
                );
            }
            else
            {
                var deleteObjectsRequest = new DeleteObjectsRequest
                {
                    BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                    Objects = objectList.S3Objects
                        .Where(t => t.Key != $"issues_historical/snapshot_date={snapshot_date}")
                        .Select(t => new KeyVersion { Key = t.Key }).ToList()
                };
                await _s3Client.DeleteObjectsAsync(deleteObjectsRequest);
            }
            _logger.LogDebug("Putting historical json file to bucket");
            var putRequest = new PutObjectRequest
            {
                BucketName = Utils.GetTextFromJson(_settings.AWS, "BucketGitlabData"),
                Key = $"issues_historical/snapshot_date={snapshot_date}/{DateTime.UtcNow.Ticks}.json",
                ContentBody = content
            };
            await _s3Client.PutObjectAsync(putRequest);
            _logger.LogDebug("Finished putting historical data to bucket");
        }
    }
}