using System;
using System.Threading.Tasks;
using Amazon.Athena;
using Amazon.Athena.Model;
using Amazon.Runtime;

namespace Barigui.Services.GitlabScrapper
{
    public class Athena
    {
        private AppSettings _settings;
        private readonly Logger _logger;
        private static AmazonAthenaClient _athenaClient;

        public Athena(Logger logger, AppSettings settings)
        {
            _settings = settings;
            _logger = logger;
            var credentials = new StoredProfileAWSCredentials(Utils.GetTextFromJson(_settings.AWS, "Profile"));
            _athenaClient = new AmazonAthenaClient(credentials);
        }
        public async Task CreateAthenaTablePartition(string database, string tableName, string partitionName)
        {
            _logger.LogDebug("Adding partition to athena table");
            // The QueryExecutionContext allows us to set the Database.
            var queryExecutionContext = new QueryExecutionContext() { Database = database };
            var resultConfiguration = new ResultConfiguration()
            {
                OutputLocation = $"s3://{Utils.GetTextFromJson(_settings.AWS, "BucketAthena")}"
            };
            // Create the StartQueryExecutionRequest to send to Athena which will start the query.
            var startQueryExecutionRequest = new StartQueryExecutionRequest()
            {
                QueryString = $"ALTER TABLE {tableName} ADD PARTITION (snapshot_date='{partitionName}')",
                QueryExecutionContext = queryExecutionContext,
                ResultConfiguration = resultConfiguration
            };
            var response = await _athenaClient.StartQueryExecutionAsync(startQueryExecutionRequest);
            _logger.LogDebug($"Adding athena table partition. QueryExecutionId: {response.QueryExecutionId}");
            var getQueryExecutionRequest = new GetQueryExecutionRequest()
            {
                QueryExecutionId = response.QueryExecutionId
            };
            bool isQueryStillRunning = true;
            while (isQueryStillRunning)
            {
                var getQueryExecutionResponse = await _athenaClient.GetQueryExecutionAsync(getQueryExecutionRequest);
                var queryState = getQueryExecutionResponse.QueryExecution.Status.State;
                if (queryState == QueryExecutionState.FAILED)
                {
                    var reason = getQueryExecutionResponse.QueryExecution.Status.StateChangeReason;
                    throw new Exception("Query Failed to run with Error Message: " + reason);
                }
                else if (queryState == QueryExecutionState.CANCELLED)
                    throw new Exception("Query was cancelled.");
                else if (queryState == QueryExecutionState.SUCCEEDED)
                    isQueryStillRunning = false;
                else
                {
                    // Sleep an amount of time before retrying again.
                    await Task.Delay(500);
                }
                _logger.LogDebug($"Current Status is: {queryState}");
            }
        }
    }

}
