using System;
using System.Threading.Tasks;

namespace Barigui.Services.GitlabScrapper
{
    public interface IApplication
    {
        Task Run();
    }
}